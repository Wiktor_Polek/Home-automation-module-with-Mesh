 /* 
 ******************************************************************************
  * File Name          : GPIO_my.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
*/
#include "GPIO_my.h"
#include "Settings.h"

extern volatile uint8 gpio_status_actual; 
extern volatile uint8 rising_edge;
extern volatile uint8 any_edge;
extern volatile bool isDark;

void GPIO_init()
{
    pinMode(LED_pin, OUTPUT);

    pinMode(Input0, INPUT_PULLUP);
    pinMode(Input1, INPUT_PULLUP);
    pinMode(Input2, INPUT_PULLUP);

    pinMode(motion_sensor_input, INPUT_PULLUP);

    pinMode(Output0, OUTPUT);
    pinMode(Output1, OUTPUT);
}

void GPIO_loop()
{
  uint8 gpio_status_old = gpio_status_actual;

  gpio_status_actual = (!digitalRead(Input0));
  gpio_status_actual |= (!digitalRead(Input1) << 1);
  gpio_status_actual |= (!digitalRead(Input2) << 2);
  gpio_status_actual |= (digitalRead(motion_sensor_input) << 3); 

rising_edge = gpio_status_actual & ~(gpio_status_old);
any_edge = gpio_status_actual ^ gpio_status_old;
}

void Photo_loop()
{
  if (analogRead(photo_sensor_input) > 500) isDark = true;
  else isDark = false;
}

void LED_blink()
{
    digitalWrite(LED_pin, !digitalRead(LED_pin)); 
}