 /* 
 ******************************************************************************
  * File Name          : main.cpp
  * Description        : This file provides code of application.
  ******************************************************************************
*/
#include <Arduino.h>
#include "painlessMesh.h" 
#include "ArduinoJson.h"
#include "GPIO_my.h"
#include "Timers.h"
#include "Settings.h"
#include "Parameters.h"
#include <DallasTemperature.h>

//Mesh functions declaration
void sendMessage();
void receivedCallback( uint32_t from, String &msg );  
void newConnectionCallback(uint32_t nodeId) ;
void changedConnectionCallback();
void nodeTimeAdjustedCallback(int32_t offset);

//Objects definition
DallasTemperature sensor(&oneWire);
Timers <3> timer;
Task taskSendMessage( TASK_SECOND * 1 , TASK_FOREVER, &sendMessage );
Module Tab_mod[Number_of_modules];
Module Tab_mod_old[Number_of_modules];
Scheduler myScheduler; 
painlessMesh  mesh_object;


/* ---------------------------------
--------------SETUP------------------
------------------------------------*/

void setup() {
  Serial.begin(9600); //initialization of UART

  for (int i = 0; i < Number_of_modules; i++) 
  {
    Tab_mod[i] = {0,0,0,0,0,0,0,0};
    Tab_mod_old[i] = Tab_mod[i];
  }
  
  GPIO_init();
  
  timer.attach(0,1000,LED_blink);           //Timer 0 initialization, 1s period, LED_blink()
  timer.attach(1,200,GPIO_loop);            //Timer 1 initialization, 200ms period, GPIO_loop()
  timer.attach(2, 4000, Photo_loop);       //Timer 2 initialization, 10s period, Photo_loop()
  
  //Temperature sensor init
  sensor.begin();
  sensor.setResolution(12);
  sensor.requestTemperatures();

  //Mesh init
  mesh_object.setDebugMsgTypes( ERROR | STARTUP ); 
  mesh_object.init( MESH_PREFIX, MESH_PASSWORD, &myScheduler, MESH_PORT );
  mesh_object.onReceive(&receivedCallback);
  mesh_object.onNewConnection(&newConnectionCallback);
  mesh_object.onChangedConnections(&changedConnectionCallback);
  mesh_object.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  myScheduler.addTask( taskSendMessage );
  taskSendMessage.enable();

}


/* ---------------------------------
--------------LOOP------------------
------------------------------------*/

void loop() 
{
timer.process();                            
mesh_object.update();

//Collect data from sensors and inputs
if(sensor.isConversionComplete())   //temperature_sensor data read
{
  sensor.setWaitForConversion(false); 
  sensor.requestTemperatures();
  sensor.setWaitForConversion(true);
  
  Tab_mod[Current_module - 1].temperature = sensor.getTempCByIndex(0);  
}

if (any_edge & Input0_Mask) //Input0
Tab_mod[Current_module - 1].Input0 = !Tab_mod[Current_module - 1].Input0; 
if (any_edge & Input1_Mask) //Input1
Tab_mod[Current_module - 1].Input1 = !Tab_mod[Current_module - 1].Input1;
if (any_edge & Input2_Mask) //Input2
Tab_mod[Current_module - 1].Input2 = !Tab_mod[Current_module - 1].Input2;
if (rising_edge & motion_sensor_mask)  //motion
Tab_mod[Current_module - 1].motion = 1;
else if (any_edge & (~rising_edge) & motion_sensor_mask) 
{
  Tab_mod[Current_module - 1].motion=0;
}
Tab_mod[Current_module - 1].isDark = isDark; //isDark

//Outputs control

if (Tab_mod[Current_module - 1].isDark == true && Tab_mod[Current_module - 1].motion == !Tab_mod_old[Current_module - 1].motion 
&& digitalRead(Output0) == false)
{
  millis_future = interval + millis();
  digitalWrite(Output0,true);
  auto_lights = true;
}

if (Tab_mod[Current_module - 1].Input0 == !Tab_mod_old[Current_module - 1].Input0)
{
  Tab_mod[Current_module - 1].Output0 = !Tab_mod[Current_module - 1].Output0;
  digitalWrite(Output0, !digitalRead(Output0));
}

if (Tab_mod[Current_module - 1].Input1 == !Tab_mod_old[Current_module - 1].Input1
||Tab_mod[Current_module - 1].Input0 == !Tab_mod_old[Current_module - 1].Input0)
{
    Tab_mod[Current_module - 1].Output1 = !Tab_mod[Current_module - 1].Output1;
  digitalWrite(Output1, !digitalRead(Output1));
}

if (millis() > millis_future && auto_lights == true) 
{
  auto_lights == false;
   digitalWrite(Output0,false);
}

any_edge =0;
rising_edge=0;
for (int i = 0; i < Number_of_modules; i++)  Tab_mod_old[i] = Tab_mod[i]; 

} //end main

/* -------------------------------------------------
                    Mesh functions 
 -------------------------------------------------*/ 

void sendMessage() 
{
  String msg;
  DynamicJsonDocument doc(1024);
  doc["NumberD"] = Current_module;
  doc["In0"] = Tab_mod[Current_module - 1].Input0;
  doc["In1"] = Tab_mod[Current_module - 1].Input1;
  doc["In2"] = Tab_mod[Current_module - 1].Input2;
  doc["Out0"] = Tab_mod[Current_module - 1].Output0;
  doc["Out1"] = Tab_mod[Current_module - 1].Output1;
  doc["Dark"] = Tab_mod[Current_module - 1].isDark;
  doc["temp"] = Tab_mod[Current_module - 1].temperature;
  doc["mot"] = Tab_mod[Current_module - 1].motion;
  
  serializeJson(doc,msg);
  mesh_object.sendBroadcast( msg );
  //Serial.println(msg);
  taskSendMessage.setInterval( random( TASK_SECOND * 1, TASK_SECOND * 3 ));
}

void receivedCallback(uint32_t from, String &msg) {
  String json;
  DynamicJsonDocument doc(1024);
  json = msg.c_str();
  DeserializationError error = deserializeJson(doc, json);
  if (error)
  {
    Serial.print("deserializeJson() failed: ");
    Serial.println(error.c_str());
  }
  int nr_rec_module = doc["NumberD"];
  Tab_mod[nr_rec_module - 1].Input0 =  doc["In0"];
  Tab_mod[nr_rec_module - 1].Input1 = doc["In1"];
  Tab_mod[nr_rec_module - 1].Input2 = doc["In2"];
  Tab_mod[nr_rec_module - 1].Output0 = doc["Out0"];
  Tab_mod[nr_rec_module - 1].Output0 = doc["Out1"];
  Tab_mod[nr_rec_module - 1].isDark = doc["Dark"];
  Tab_mod[nr_rec_module - 1].temperature = doc["temp"];
  Tab_mod[nr_rec_module - 1].motion = doc["mot"];
  
  Serial.println(msg);
}

void newConnectionCallback(uint32_t nodeId) {
  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}
void changedConnectionCallback() {
  Serial.printf("Changed connections\n");
}
void nodeTimeAdjustedCallback(int32_t offset) {
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh_object.getNodeTime(), offset);
}