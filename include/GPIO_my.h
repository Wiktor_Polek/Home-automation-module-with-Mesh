 /* 
 ******************************************************************************
  * File Name          : GPIO_my.h
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
*/
#ifndef _GPIO_my_h
#define _GPIO_my_h

#include <Arduino.h>

#ifdef __cplusplus
extern "C"{
#endif

void GPIO_init();
void GPIO_loop();
void Photo_loop();
void LED_blink();


#ifdef __cplusplus
} // extern "C"
#endif

#endif /* INC_GPIO_MY_H_ */