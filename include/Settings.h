 /* 
 ******************************************************************************
  * File Name          : Settings.h
  * Description        : This file provides code for all used defines.
  ******************************************************************************
*/

// ***** Define ***** //

//Wi-Fi Credentials
#define MESH_PREFIX "Moduly_Automatyki_Budynkowej"
#define MESH_PASSWORD "Ugabuga123"
#define MESH_PORT 5555
#define Number_of_modules 3
#define Current_module 1 

//PIN Declaration
#define LED_pin D4
#define Input0 D1
#define Input1 D2
#define Input2 D3

#define Output0 D6
#define Output1 D7

#define temp_sensor_input D5
#define motion_sensor_input D8 
#define photo_sensor_input A0

//PIN Mask
#define Input0_Mask  0x01
#define Input1_Mask  0x02
#define Input2_Mask  0x04
#define motion_sensor_mask  0x08
