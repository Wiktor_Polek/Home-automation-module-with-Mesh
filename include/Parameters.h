 /* 
 ******************************************************************************
  * File Name          : Parameters.h
  * Description        : This file provides code for all used 
  *                       global variables and defines.
  ******************************************************************************
*/
#include "painlessMesh.h"
#include <OneWire.h>
#include "Settings.h"

//Structure of module
struct Module{
bool Input0;
bool Input1;
bool Input2;
bool Output0;
bool Output1;

bool isDark;
float temperature;
bool motion;
};


// ***** Variables ***** //

volatile uint8 gpio_status_actual; //[D8 D3 D2 D1]
volatile uint8 rising_edge; 
volatile uint8 any_edge;

volatile bool isDark; 

volatile long millis_future; 
volatile unsigned long interval = 60000; //interval of turn-on light
volatile bool auto_lights;  

//SENSOR_TEMPEATURE
volatile float temp_ext; //temperature
OneWire oneWire(temp_sensor_input);   //declaration of pin for Onewire